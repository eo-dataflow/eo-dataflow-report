# coding=utf-8

"""
eo_dataflow_report.utils.parameters
------------------------------

This package provides a common framework for the loading of the eo dataflow
report configuration file.

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""
from __future__ import annotations

import argparse
from pathlib import Path
from typing import Optional

import yaml

import eo_dataflow_report.utils.models as models
from eo_dataflow_report.utils.arguments import get_override_args
from eo_dataflow_report.utils.configuration import EODataflowReportModel


def add_log_params(yaml_data: dict, args: argparse.Namespace) -> dict:
    yaml_data['log']['verbose'] = args.verbose
    yaml_data['log']['quiet'] = args.quiet
    yaml_data['log']['silence'] = args.silence
    yaml_data['log']['logfile'] = args.logfile
    yaml_data['log']['logfile_level'] = args.logfile_level


class LogOptions(EODataflowReportModel):
    verbose: Optional[bool] = False
    quiet: Optional[bool] = False
    silent: Optional[bool] = False
    logfile: Optional[Path]
    logfile_level: Optional[models.LogLevel] = models.LogLevel.info


class EODataflowReportModelParameters(EODataflowReportModel):
    log: LogOptions


class EODataflowReportParams(EODataflowReportModelParameters):
    pass

def get_parameters(
    params_type: EODataflowReportModel,
    args: argparse.Namespace
) -> EODataflowReportModelParameters:
    """
    Args:
        params_type:
        args:

    Returns:

    """
    yaml_data = {'log': LogOptions().dict()}

    if 'parameters_file' in args and args.parameters_file is not None:
        yaml_file = Path(args.parameters_file)
        if not yaml_file.exists():
            raise FileNotFoundError(
                'The "{0}" parameters file does not exist'.format(
                    args.parameters_file)
            )

        try:
            with open(yaml_file) as open_file:
                yaml_data = yaml.load(open_file, Loader=yaml.SafeLoader)
        except yaml.YAMLError:
            raise

    if args.override_params is not None:
        yaml_data = get_override_args(yaml_data, args.override_params)
    return params_type(**yaml_data)
