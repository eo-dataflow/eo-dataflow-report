import copy
from typing import Mapping


def deep_merge_dicts(base: Mapping, addition: Mapping) -> Mapping:
    """Merge two dictionaries.

    Args:
        base: main dictionary
        addition: additional information to add

    Returns:
        Mapping: new dictionary
    """
    result = dict(copy.deepcopy(base))
    for key, value in addition.items():
        if isinstance(value, dict) and key in base and isinstance(base[key], dict):
            result[key] = deep_merge_dicts(base[key], value)
        else:
            result[key] = value
    return result
