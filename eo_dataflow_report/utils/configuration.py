"""EO dataflow report configuration."""
from __future__ import annotations

import logging
import os
from enum import Enum
from pathlib import Path
from typing import Dict, List, Union

import yaml
from pydantic import AnyHttpUrl, BaseModel, BaseSettings

from eo_dataflow_report.utils.exceptions import EODataflowReportError
from eo_dataflow_report.utils.merger import deep_merge_dicts

logger = logging.getLogger('eo_dataflow.report')

# Environment variable for EO dataflow configuration file path
EO_DATAFLOW_REPORT_CFG_ENV = 'EO_DATAFLOW_REPORT_CFG'


class EODataflowReportSettings(BaseSettings):
    class Config:  # noqa: D106, WPS431
        env_prefix = 'eo_dataflow_report_'
        validate_all = True
        validate_assignment = True

    @classmethod
    def from_yaml_file(cls, file: Path):
        """"Load a model from an YAML file"""
        with open(file) as f:
            return cls(**yaml.safe_load(f))

    def to_yaml(self) -> str:
        """"Dump model to YAML"""
        return yaml.dump(self.dict())

    def merge(self, partial: Dict) -> EODataflowReportSettings:
        """Merge from a partial dictionary"""
        if partial is None:
            return self
        merge_dicts = deep_merge_dicts(self.dict(), partial)
        return self.parse_obj(merge_dicts)


class EODataflowReportModel(BaseModel):
    def merge(self, partial: Dict) -> EODataflowReportModel:
        """Merge from a partial dictionary"""
        merge_dicts = deep_merge_dicts(self.dict(), partial)
        return self.parse_obj(merge_dicts)

    class Config:
        """Validate constraints on assignment"""
        validate_all = True
        validate_assignment = True


class ConfigException(Exception):
    pass


class ElasticSearchOperationConfig(EODataflowReportModel):
    """
    Class containing the Elasticsearch operation configuration model
    """
    chunksize: int = 5000
    timeout: str = '10s'
    scroll_retention_time: str = '5m'


class ElasticSearchConfig(EODataflowReportModel):
    """
    Class containing the Elasticsearch configuration model
    """
    url: AnyHttpUrl = 'http://127.0.0.1:9200'
    prefix: str = 'eo_dataflow_report_'
    chunksize: int = 5000
    timeout: str = '10s'
    scroll_retention_time: str = '5m'
    operations: Union[Dict[str, ElasticSearchOperationConfig], None] = None

    def get_operation_config(self, operation_key: str) \
            -> ElasticSearchOperationConfig:
        """ Return the ES configuration for a given ES operation """
        # Create a configuration with global ES settings
        config = ElasticSearchOperationConfig.parse_obj(
            {'scroll_retention_time': self.scroll_retention_time,
             'timeout': self.timeout,
             'chunksize': self.chunksize}
        )
        # Check if there are specific settings
        # defined for this operation in ES config
        if self.operations is not None and operation_key in self.operations:
            operation_config = self.operations[operation_key]
            # Merge default config and operation settings
            config = config.copy(
                update=operation_config.dict(exclude_none=True)
            )

        return config


class GrafanaDownloadMonitoringConfig(EODataflowReportModel):
    """Eo dataflow download monitoring configuration."""

    alert_threshold_nb_of_files_downloaded_per_sensing_day: int = -1
    alert_threshold_nb_of_files_downloaded_per_day: int = -1


class GrafanaDownloadConfig(EODataflowReportModel):
    """Eo dataflow download configuration."""

    name: str
    monitoring: GrafanaDownloadMonitoringConfig = GrafanaDownloadMonitoringConfig()


class GrafanaDashboardGroup(EODataflowReportModel):
    """Eo dataflow Grafana dashboard group."""

    name: str = 'cwwic cfosat'
    downloads: List[GrafanaDownloadConfig] = [
        GrafanaDownloadConfig(name='cwwic_cfosat_swisca_l2s'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l1a'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l1b'),
        GrafanaDownloadConfig(name='cwwic_cfosat_aux_meteo'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l1bexp'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l2'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l2_exp'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l2anad'),
        GrafanaDownloadConfig(name='cwwic_cfosat_sca_l1b'),
        GrafanaDownloadConfig(name='cwwic_cfosat_sca_nrt'),
        GrafanaDownloadConfig(name='cwwic_cfosat_sca_l2a'),
        GrafanaDownloadConfig(name='cwwic_cfosat_swi_l1altm'),
        GrafanaDownloadConfig(name='cwwic_cfosat_aux_global'),
    ]


class GrafanaDashboardsConfiguration(EODataflowReportModel):
    """Represents the Grafana dashboards configuration model."""

    plugin_version: str = '9.3.2'
    time_range_from_days: int = 90
    time_range_to_days: int = 0
    groups: List[GrafanaDashboardGroup] = [
        GrafanaDashboardGroup(),
        GrafanaDashboardGroup(
            name='sentinel',
            downloads=[
                GrafanaDownloadConfig(name='esa_s1b_wv_ocn'),
                GrafanaDownloadConfig(name='esa_s1a_wv_ocn'),
                GrafanaDownloadConfig(name='coda_s3b_sl_2_wst____o_nr'),
                GrafanaDownloadConfig(name='coda_s3a_sl_2_wst____o_nr'),
                GrafanaDownloadConfig(name='coda_s3b_sr_2_wat'),
                GrafanaDownloadConfig(name='OS_coda_s3a_sr_2_wat'),
            ]
        )]


class GrafanaSearchConfiguration(EODataflowReportModel):
    """Represents the Grafana search configuration model."""

    dashboard_type: str = 'dash-db'


class GrafanaDatasourcesJsonDataConfiguration(EODataflowReportModel):
    """Represents the Grafana datasources json data configuration model."""

    es_version: str = '7.10.0'
    include_frozen: bool = False
    max_concurrent_shard_requests: int = 5
    time_interval: str = '1d'


class GrafanaDatasourcesConfiguration(EODataflowReportModel):
    """Represents the Grafana datasources configuration model."""

    type: str = 'elasticsearch'
    access: str = 'proxy'
    url: AnyHttpUrl = 'http://127.0.0.1:9200'
    password: str = 'es_user_password'
    user: str = 'es_user_login'
    basic_auth: bool = True
    time_field_name: str = 'sensing_time'
    json_data: GrafanaDatasourcesJsonDataConfiguration = GrafanaDatasourcesJsonDataConfiguration()


class GrafanaAlertRulesConfiguration(EODataflowReportModel):
    """Represents the Grafana alert rules configuration model."""
    # How often the alert will be evaluated to see if it fires
    alert_evaluation_period: str = '1m'
    # Once condition is breached, alert will go into pending state.
    # If it is pending for longer than the "for" value, it will become a
    # firing alert.
    alert_firing_delay: str = '5m'
    # The relative timerange start in seconds from now of alert analysis
    alert_relative_timerange_start: int = 864000
    # The relative timerange end in seconds from now of alert analysis
    alert_relative_timerange_end: int = 0


class GrafanaConfiguration(EODataflowReportModel):
    """Represents the Grafana configuration model."""

    host: str = 'grafana.domain.fr'
    api_key: str = 'api_key'
    ssl_verify: bool = False
    search: GrafanaSearchConfiguration = GrafanaSearchConfiguration()
    alert_rules: GrafanaAlertRulesConfiguration = GrafanaAlertRulesConfiguration()
    datasources: GrafanaDatasourcesConfiguration = GrafanaDatasourcesConfiguration()
    dashboards: GrafanaDashboardsConfiguration = GrafanaDashboardsConfiguration()
    root_folder: str = 'eo_dataflow_report'


class RepeatIntervalUnitEnum(str, Enum):
    """EO dataflow report mail notification repeat interval unit enumeration."""

    seconds = 's'
    minutes = 'm'
    hours = 'h'
    days = 'd'
    weeks = 'w'


class RepeatIntervalConfig(EODataflowReportModel):
    """EO dataflow report mail notification repeat interval configuration."""

    duration: int = 4
    unit: RepeatIntervalUnitEnum = RepeatIntervalUnitEnum.hours


class ContactPointConfig(EODataflowReportModel):
    """EO dataflow report contact point configuration."""

    name: str
    addresses: List[str]
    disable_resolve_message: bool
    single_email: bool
    repeat_interval: RepeatIntervalConfig = RepeatIntervalConfig()


class HarvestingDelayUnits(str, Enum):
    """Harvesting delay unit enumeration."""

    seconds = 'seconds'
    minutes = 'minutes'
    hours = 'hours'
    days = 'days'


class HarvestingDelay(EODataflowReportModel):
    """Downloader metrics files harvesting delay."""
    value: int = 10
    unit: HarvestingDelayUnits = HarvestingDelayUnits.minutes


class EODataflowReportConfig(EODataflowReportSettings):
    """EO dataflow report configuration."""

    contact_points: List[ContactPointConfig] = []
    input_paths: Union[Path, List[Path]] = None
    harvesting_delay: HarvestingDelay = HarvestingDelay()
    workspace_path: Path = None
    elasticsearch: ElasticSearchConfig = ElasticSearchConfig()
    grafana: GrafanaConfiguration = GrafanaConfiguration()


def load_eo_dataflow_report_config(
    configuration_file: Path = None,
    override_data: Dict = None
) -> EODataflowReportConfig:
    """Load EO dataflow report configuration from a YAML File.

    Args:
        configuration_file: YAML configuration file
        override_data: override data

    Returns:
        EODataflowReportConfig: an instance of EODataflowReportConfig
    """
    if configuration_file is None and EO_DATAFLOW_REPORT_CFG_ENV in os.environ:
        configuration_file = Path(os.environ[EO_DATAFLOW_REPORT_CFG_ENV])
        if not configuration_file.exists():
            raise EODataflowReportError(
                """
                The file {0} (provided by the environmental variable {1}
                is not available. As this file provides the
                configuration for this eo dataflow report instance, the program
                will now exit. Please read the eo dataflow report documentation
                and ensure that your ao dataflow report instance is correctly
                installed.
                """
                .format(os.environ[EO_DATAFLOW_REPORT_CFG_ENV],
                        EO_DATAFLOW_REPORT_CFG_ENV)
            )

    process_config = EODataflowReportConfig()
    if configuration_file is not None:
        process_config = EODataflowReportConfig.from_yaml_file(
            configuration_file)
    # TODO voir si un bug (et aussi felyx_report, felyx_processor)
    # process_config.merge(override_data)
    # return process_config
    return process_config.merge(override_data)
