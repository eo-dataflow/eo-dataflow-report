# coding=utf-8

"""
eo_datalow_report.utils.arguments
------------------------------

This package provides a common framework for setting up logging

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""
import errno
import logging
import os
import sys
from pathlib import Path

LOG_FORMAT = '%(asctime)s|%(levelname)-8s|%(message)-100s|' \
             '%(filename)s:%(lineno)d@%(funcName)s()'
logger = logging.getLogger('eo_dataflow.report')


def makedirs(dir_path: str) -> bool:
    """"""
    full_path = os.path.abspath(dir_path)
    if os.path.exists(full_path):
        _msg = '{} already exists. Skipping directory creation'
        logger.debug(_msg.format(full_path))
        return True

    try:
        os.makedirs(full_path)
    except OSError:
        _, e, _ = sys.exc_info()
        if e.errno != errno.EEXIST:
            # Error was not due to a simple race with another process which
            # already created the directory => raise exception
            _msg = 'Exception raised while creating {}'
            logger.error(_msg.format(full_path), exc_info=e)
            return False
    return True


def build_logfile_handler(
        logfile_path: Path,
        logfile_level: str
) -> logging.FileHandler:
    """

    :param logfile_path: File path
    :param logfile_level: Log level
    :return: File Handler for logging
    """
    abs_path = logfile_path.resolve()
    logdir = abs_path.parent
    ok = makedirs(logdir)
    if not ok:
        logger.error('Could not create parent directory for the logfile')
        return None

    file_handler = logging.FileHandler(abs_path, mode='a', encoding='utf-8',
                                       delay=True)
    if 'debug' == logfile_level:
        file_handler.setLevel(logging.DEBUG)
    elif 'info' == logfile_level:
        file_handler.setLevel(logging.INFO)
    elif 'warning' == logfile_level:
        file_handler.setLevel(logging.WARNING)
    elif 'error' == logfile_level:
        file_handler.setLevel(logging.ERROR)
    else:
        logger.error('Unknown log level "{}"'.format(logfile_level))
        return None

    return file_handler


def setup_logging(
    app_logger: logging.Logger,
    settings  # : eo_dataflow_report.utils.parameters.LogOptions
) -> bool:
    """

    :param app_logger: Main logger
    :param settings: log paramterscat
    :return: True if setup OK
    """

    app_logger.setLevel(logging.DEBUG)
    app_logger.handlers = []
    handler = logging.StreamHandler()
    formatter = logging.Formatter(fmt=LOG_FORMAT)
    handler.setFormatter(formatter)
    app_logger.addHandler(handler)

    if settings.verbose:
        handler.setLevel(logging.DEBUG)
    elif settings.quiet:
        handler.setLevel(logging.WARNING)
    elif settings.silent:
        handler.setLevel(logging.FATAL)
    else:
        handler.setLevel(logging.INFO)

    if settings.logfile is not None:
        file_handler = build_logfile_handler(
            settings.logfile,
            settings.logfile_level)
        if file_handler is None:
            _msg = 'Could not create a handler to log messages to "{}"'
            logger.error(_msg.format(settings.logfile))
            return False
        file_handler.setFormatter(formatter)
        app_logger.addHandler(file_handler)

    return True
