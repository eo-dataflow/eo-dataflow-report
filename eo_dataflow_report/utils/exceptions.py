# encoding: utf-8
"""
exceptions
--------------------------------------

Contains classes for eo dataflow report exceptions

"""

#TODO : Exceptions used in es_client : create a base class ESClientError and
# a base class Exception without reference to Felyx (do the same for
# felyx_processor and felyx_report projects


class FelyxProcessorError(Exception):
    """Base class for felyx processor exceptions """


class UnknownOperatorError(FelyxProcessorError):
    """Unknown operator exception """


class QueryToJsonError(FelyxProcessorError):
    """Conversion query to JSON exception """


class ElasticsearchError(FelyxProcessorError):
    """Elasticsearch client exception """


class EODataflowReportError(Exception):
    """Base class for eo dataflow report exceptions """


class TemplatingError(EODataflowReportError):
    """Templating error."""


class ConversionError(EODataflowReportError):
    """Conversion Error."""
