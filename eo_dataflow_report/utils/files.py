"""EO dataflow files utils."""

import time
from pathlib import Path
from typing import Optional

from eo_dataflow_report.utils.exceptions import ConversionError


def mtime_older_than(
    file_path: Path,
    delta_value: Optional[int] = 10,
    delta_unit: Optional[str] = 'minutes'
) -> bool:
    """Checks if file mtime is older than a delta time.

    Args:
        file_path: The file path.
        delta_value: The delta value.
        delta_unit: The delta unit amongst [seconds, minutes, hours, days].

    Returns:
        True if file mtime is older than the delta time, otherwise False.

    Raises:
        ConversionError : invalid delta unit
    """
    conversion_dict = {'seconds': 1, 'minutes': 60, 'hours': 3600, 'days': 86400}
    try:
        factor = conversion_dict[delta_unit]
        return (time.time() - file_path.stat().st_mtime / factor) >= delta_value
    except KeyError:
        raise ConversionError('"{0}" is not a valid time unit. Time units list : {1}'.format(
                delta_unit,
                ', '.join(conversion_dict.keys()),
            )
        )
