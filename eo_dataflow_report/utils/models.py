# coding=utf-8

"""
eo_dataflow_report.utils.models
------------------------------

This package provides a common framework for data structures and their validation.

.. :copyright:
.. :license:

.. sectionauthor::
.. codeauthor::
"""

from enum import Enum


class LogLevel(str, Enum):
    """
    Class containing the LogLevel
    """
    debug = 'debug'
    info = 'info'
    warning = 'warning'
    error = 'error'


class LogLevel(str, Enum):
    """
    Class containing the LogLevel
    """
    debug = 'debug'
    info = 'info'
    warning = 'warning'
    error = 'error'
