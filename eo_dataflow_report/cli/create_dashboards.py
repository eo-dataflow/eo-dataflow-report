"""EO dataflow create dashboards main command.

This command is used to create dashboards from metrics indexed into
elasticsearch.
The input is a configuration file.
"""

import argparse
import logging
import sys
from typing import List, NoReturn, Tuple

from eo_dataflow_report.report.monitor import dashboard, datasource
from eo_dataflow_report.utils.arguments import (
    get_override_args,
    parse_all_args)
from eo_dataflow_report.utils.configuration import (
    EODataflowReportConfig,
    load_eo_dataflow_report_config)
from eo_dataflow_report.utils.logging import setup_logging
from eo_dataflow_report.utils.parameters import (
    EODataflowReportParams,
    get_parameters)

logger = logging.getLogger('eo_dataflow.create_dashboards')


def parse_args(  # noqa: WPS213
        cli_args: List[str],
) -> Tuple[EODataflowReportConfig, EODataflowReportParams]:
    """Parse the command line arguments.

    Args:
        cli_args: The command line arguments to be parsed.

    Returns:
        A tuple containing the eo dataflow configuration and the command
        parameters.

    Raises:
        FileNotFoundError: If the configuration file does not exist.
    """
    report_parser = argparse.ArgumentParser(
        description='Produce monitoring report from data processed by the eo dataflow processing',
    )
    report_parser.add_argument(
        '-c',
        '--configuration_file',
        default=None,
        help='Configuration file path',
    )
    report_parser.add_argument(
        '--logfile',
        default=None,
        help='Path of the file where logs will be written',
    )
    report_parser.add_argument(
        '--logfile_level',
        default=None,
        help='Minimal level that log messages must reach to be written in the log file',
    )
    level_group = report_parser.add_mutually_exclusive_group()
    level_group.add_argument(
        '-v',
        '--verbose',
        action='store_true',
        help='Activate debug level logging - for extra feedback.',
    )
    level_group.add_argument(
        '-q',
        '--quiet',
        action='store_true',
        help='Disable information logging - for reduced feedback.',
    )
    level_group.add_argument(
        '-s',
        '--silence',
        action='store_true',
        help='Log ONLY the most critical or fatal errors.',
    )
    args = parse_all_args(report_parser, cli_args)

    try:
        process_config = load_eo_dataflow_report_config(
            configuration_file=args.configuration_file,
            override_data=get_override_args(override_data=args.override_config),
        )
        process_params = get_parameters(
            params_type=EODataflowReportParams,
            args=args
        )
        return process_config, process_params
    except FileNotFoundError:
        raise FileNotFoundError('{0} configuration file does not exist'.format(
            args.configuration_file))
    except Exception as exception:
        logger.exception(exception)
        logger.exception('Done (failure)')
        sys.exit(1)


def exit_success() -> NoReturn:
    """Exit command with success."""
    logger.info('Done (success)')
    sys.exit(0)


def exit_failure() -> NoReturn:
    """Exit command with failure."""
    logger.error('Done (failure)')
    sys.exit(1)


def create_dashboards(cli_args=None):
    """Create eo dataflow dashboards.

    Args:
        cli_args: The command line arguments
    """

    # Retrieve configuration and command line arguments
    main_logger = logging.getLogger()

    process_config, process_params = parse_args(cli_args)
    if not setup_logging(main_logger, process_params.log):
        exit_failure()

    logger.info('Start of eo_dataflow_create_dashboards')

    # Create Grafana datasource
    datasource.create_eo_dataflow_datasource(
        es_config=process_config.elasticsearch,
        grafana_config=process_config.grafana,
    )

    # Create Grafana dashboards
    dashboard.process(
        grafana_config=process_config.grafana,
        es_prefix=process_config.elasticsearch.prefix,
        contact_point_configs=process_config.contact_points
    )

    logger.info('End of eo_dataflow_create_dashboards')

    exit_success()


if __name__ == '__main__':
    create_dashboards()
