"""Create eo_dataflow_report datasource."""

from typing import NoReturn

from eo_dataflow_report.monitoring.grafana.api_client import GrafanaAPIClient
from eo_dataflow_report.utils.configuration import (
    ElasticSearchConfig,
    GrafanaConfiguration)


def create_eo_dataflow_datasource(
    es_config: ElasticSearchConfig,
    grafana_config: GrafanaConfiguration,
) -> NoReturn:
    """Create the Grafana eo_data_flow datasource and the root folder if not existing.

    Args:
        es_config: The Elasticsearch configuration
        grafana_config: The Grafana configuration
    """
    grafana_api_client = GrafanaAPIClient(grafana_config=grafana_config)
    if not grafana_api_client.folder_exists(folder_name=grafana_config.root_folder):
        grafana_api_client.create_root_folder()
    if not grafana_api_client.datasource_exists(name=es_config.prefix):
        grafana_api_client.create_grafana_es_datasource(
            name=es_config.prefix,
            es_index_name='{0}_*'.format(es_config.prefix),
            time_field=grafana_config.datasources.time_field_name,
        )
