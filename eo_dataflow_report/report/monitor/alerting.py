"""Handle Grafana alert rules."""

from pathlib import Path
from typing import Any, Dict, NoReturn

from eo_dataflow_report.monitoring.grafana.api_client import GrafanaAPIClient
from eo_dataflow_report.report.monitor import notifying
from eo_dataflow_report.utils.configuration import (
    GrafanaDownloadMonitoringConfig,
    RepeatIntervalConfig)
from eo_dataflow_report.utils.templating import render_json_template


def _create_nb_of_files_downloaded_per_sensing_day_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_files_dl_per_sensing_day.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def _create_nb_of_files_downloaded_per_day_rule(
    grafana_api_client: GrafanaAPIClient,
    template_dir: Path,
    template_params: Dict[str, Any],
):
    alert_rule_rendered = render_json_template(
        template_dir=template_dir,
        template_name='nb_of_files_dl_per_day.tpl',
        template_params=template_params,
    )
    grafana_api_client.create_alert_rule(alert_rule=alert_rule_rendered)


def delete_alert_rules(grafana_api_client: GrafanaAPIClient, dashboard_uid: str) -> NoReturn:
    """Delete alert rules by dashboard uid (label).

    Args:
        grafana_api_client: The Grafana API client
        dashboard_uid: The dashboard uid
    """
    grafana_api_client.delete_alert_rules(dashboard_uid=dashboard_uid)


def create_alert_rules(
    grafana_api_client: GrafanaAPIClient,
    datasource_uid: str,
    group_name: str,
    download_name: str,
    dashboard_uid: str,
    download_monitoring_config: GrafanaDownloadMonitoringConfig,
    contact_points_dict: Dict[str, RepeatIntervalConfig],
) -> NoReturn:
    """Create Grafana alert rules.

    Args:
        grafana_api_client: The Grafana API client
        datasource_uid: The Grafana datasource uid
        group_name: the dashboard group name
        download_name: The download name
        dashboard_uid: The dashboard uid
        download_monitoring_config : The download monitoring configuration
        contact_points_dict: The contact points dictionary
    """

    template_dir = Path(Path(__file__).parent.parent.parent, 'share', 'grafana', 'alert_rules')

    common_alert_params = {
        'timeField': grafana_api_client.config.datasources.time_field_name,
        'datasourceUid': datasource_uid,
        'group_name': group_name,
        'download_name': download_name,
        'alertIntervall':
            grafana_api_client.config.alert_rules.alert_evaluation_period,
        'alertFor':
            grafana_api_client.config.alert_rules.alert_firing_delay,
        'alertTimeRangeFrom':
            grafana_api_client.config.alert_rules.alert_relative_timerange_start,
        'alertTimeRangeTo':
            grafana_api_client.config.alert_rules.alert_relative_timerange_end,
        'dashboard_uid': dashboard_uid,
    }

    alert_threshold_nb_of_files_downloaded_per_sensing_day = \
        download_monitoring_config.alert_threshold_nb_of_files_downloaded_per_sensing_day
    if alert_threshold_nb_of_files_downloaded_per_sensing_day != -1:
        _create_nb_of_files_downloaded_per_sensing_day_rule(
            grafana_api_client=grafana_api_client,
            template_dir=template_dir,
            template_params={
                **common_alert_params,
                **{'alert_threshold': alert_threshold_nb_of_files_downloaded_per_sensing_day}
            }
        )
        notifying.create_notification_policies(
            grafana_api_client=grafana_api_client,
            download_name=download_name,
            dashboard_uid=dashboard_uid,
            contact_points_dict=contact_points_dict,
        )

    alert_threshold_nb_of_files_downloaded_per_day = \
        download_monitoring_config.alert_threshold_nb_of_files_downloaded_per_day
    if alert_threshold_nb_of_files_downloaded_per_day != -1:
        _create_nb_of_files_downloaded_per_day_rule(
            grafana_api_client=grafana_api_client,
            template_dir=template_dir,
            template_params={
                **common_alert_params,
                **{'alert_threshold': alert_threshold_nb_of_files_downloaded_per_day}
            }
        )
        notifying.create_notification_policies(
            grafana_api_client=grafana_api_client,
            download_name=download_name,
            dashboard_uid=dashboard_uid,
            contact_points_dict=contact_points_dict,
        )
