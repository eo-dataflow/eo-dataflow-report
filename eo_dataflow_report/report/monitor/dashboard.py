"""Create eo_dataflow_report dashboards."""

import json
from pathlib import Path
from typing import List, NoReturn, Optional

from eo_dataflow_report.monitoring.grafana.api_client import GrafanaAPIClient
from eo_dataflow_report.report.monitor import alerting, notifying
from eo_dataflow_report.utils.configuration import (
    ContactPointConfig,
    GrafanaConfiguration)
from eo_dataflow_report.utils.exceptions import EODataflowReportError
from eo_dataflow_report.utils.templating import render_json_template


def _render_templating_list_options(
    template_dir: Path,
    downloads_list: List[str],
) -> List[str]:
    templating_list_options_rendered = []
    for download_name in downloads_list:
        templating_list_options_rendered.append(
            render_json_template(
                template_dir=template_dir,
                template_name='templating_list_option.tpl',
                template_params={'download_name': download_name},
            )
        )
    return templating_list_options_rendered


def create_eo_dataflow_report_dashboard(
    grafana_api_client: GrafanaAPIClient,
    group_name: str,
    downloads_list: List[str],
    datasource_uid: str,
    overwrite: bool,
    dashboard_uid: Optional[str] = None,
) -> str:
    """Create the Grafana eo_dataflow_report dashboards.

    Args:
        grafana_api_client: The Grafana API client
        group_name: The download group name
        downloads_list: The downloads list
        datasource_uid: The Grafana datasource uid
        overwrite: Boolean indicating whether the dashboard should be overwritten
        dashboard_uid: The existing dashboard uid in case of update

    Returns:
        The dashboard uid
    """
    grafana_config = grafana_api_client.config
    template_dir = Path(Path(__file__).parent.parent.parent, 'share', 'grafana', 'dashboard')

    time_range_from_days = grafana_api_client.config.dashboards.time_range_from_days
    time_range_to_days = grafana_api_client.config.dashboards.time_range_to_days

    templating_list_options_rendered = _render_templating_list_options(
        template_dir=template_dir,
        downloads_list=downloads_list,
    )

    sub_templates_params = {
        'timeField': grafana_config.datasources.time_field_name,
        'datasourceType': grafana_config.datasources.type,
        'datasourceUid': datasource_uid,
    }

    full_template_params = {
        'group_name': group_name,
        'folderId': grafana_api_client.get_root_folder_id(),
        'overwrite': overwrite,
        'dashboardTimeRangeFrom': time_range_from_days,
        'dashboardTimeRangeTo': time_range_to_days,
        'downloads_list': ',\n'.join(downloads_list),
        'templating_list_options': ','.join(templating_list_options_rendered),
        'nb_of_files_dl_per_sensing_day': render_json_template(
            template_dir=template_dir,
            template_name='nb_of_files_dl_per_sensing_day.tpl',
            template_params=sub_templates_params,
        ),
        'nb_of_files_dl_per_day': render_json_template(
            template_dir=template_dir,
            template_name='nb_of_files_dl_per_day.tpl',
            template_params=sub_templates_params,
        ),
        'avg_delay_btw_sensing_time_and_dl_time': render_json_template(
            template_dir=template_dir,
            template_name='avg_delay_btw_sensing_time_and_dl_time.tpl',
            template_params=sub_templates_params,
        ),
        'max_delay_btw_file_mod_and_dl_time': render_json_template(
            template_dir=template_dir,
            template_name='max_delay_btw_file_mod_and_dl_time.tpl',
            template_params=sub_templates_params,
        ),
    }

    if dashboard_uid:
        full_template_params = {
            **full_template_params,
            **{'dashboard_uid': dashboard_uid},
        }

    return grafana_api_client.create_dashboard(
        dashboard_json=json.loads(
            render_json_template(
                template_dir=template_dir,
                template_name='eo_dataflow_report_dashboard.tpl',
                template_params=full_template_params,
            ),
        ),
    )


def process(
    grafana_config: GrafanaConfiguration,
    es_prefix: str,
    contact_point_configs: List[ContactPointConfig],
) -> NoReturn:
    """Create the Grafana dashboards if not already existing.

    Args:
        grafana_config: The Grafana configuration
        es_prefix: The Elasticsearch index prefix
        contact_point_configs: The contact points configurations
    """
    grafana_api_client = GrafanaAPIClient(grafana_config=grafana_config)
    try:
        datasource_uid = grafana_api_client.client.datasource.get_datasource_by_name(es_prefix)['uid']
    except Exception as exception:
        raise EODataflowReportError(
            'Unable to retrieve uid with datasource name {0} : {1}'.format(es_prefix, exception),
        )

    contact_points_dict = notifying.create_contact_points(
        grafana_api_client=grafana_api_client,
        contact_point_configs=contact_point_configs,
    )

    for dashboard_group in grafana_config.dashboards.groups:
        group_name = dashboard_group.name
        download_names_list = [download.name for download in dashboard_group.downloads]
        dashboard_name = 'eo-dataflow {0}'.format(group_name)
        dashboard_uid = None
        if grafana_api_client.dashboard_exists(dashboard_name=dashboard_name):
            dashboard_uid = grafana_api_client.get_dashboard_uid_by_name(dashboard_name=dashboard_name)
            grafana_api_client.delete_notification_policies(contact_point_names=list(contact_points_dict.keys()))
            alerting.delete_alert_rules(grafana_api_client=grafana_api_client, dashboard_uid=dashboard_uid)

        if dashboard_uid:
            create_eo_dataflow_report_dashboard(
                grafana_api_client=grafana_api_client,
                group_name=group_name,
                downloads_list=download_names_list,
                datasource_uid=datasource_uid,
                dashboard_uid=dashboard_uid,
                overwrite=True,
            )
        else:
            dashboard_uid = create_eo_dataflow_report_dashboard(
                grafana_api_client=grafana_api_client,
                group_name=group_name,
                downloads_list=download_names_list,
                datasource_uid=datasource_uid,
                overwrite=False,
            )

        for download in dashboard_group.downloads:
            if download.monitoring:
                alerting.create_alert_rules(
                    grafana_api_client=grafana_api_client,
                    datasource_uid=datasource_uid,
                    group_name=group_name,
                    download_name=download.name,
                    dashboard_uid=dashboard_uid,
                    download_monitoring_config=download.monitoring,
                    contact_points_dict=contact_points_dict,
                )
