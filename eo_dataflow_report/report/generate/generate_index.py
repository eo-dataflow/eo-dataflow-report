"""EO dataflow report main command.

This command is used to collect downloaded eo files metrics and index them into
elasticsearch indices.
The input is a configuration file.
"""

import datetime
import gzip
import json
import logging
import os
import re
import shutil
from pathlib import Path
from typing import Dict, List, Tuple

from eo_dataflow_report.index.es.es_client import ESClient
from eo_dataflow_report.utils import files
from eo_dataflow_report.utils.configuration import EODataflowReportConfig
from eo_dataflow_report.utils.exceptions import EODataflowReportError

# operation key described in ElasticSearchConfig
ES_REPORT_INDEX = 'report_index'

logger = logging.getLogger('eo_dataflow.report')


class EODataflowIndexer(object):
    """Implementation of the EO dataflow indexing process"""

    def __init__(self, process_config: EODataflowReportConfig):
        self.process_config = process_config
        self.input_paths = list()
        self.root_archive_path = None
        self.rejected_path = None
        self.temp_path = None
        self.es_client = None
        self._initialize()

    def _initialize(self):
        """
        Initialize the process
        """
        # Check workspace path
        if not self.process_config.workspace_path:
            raise EODataflowReportError('Workspace path is unset')

        # Check input paths
        if not self.process_config.input_paths:
            raise EODataflowReportError('No input paths')

        if not isinstance(self.process_config.input_paths, list):
            self.process_config.input_paths = [self.process_config.input_paths]

        if len(self.process_config.input_paths) == 0:
            raise EODataflowReportError('No input paths')

        for input_path in self.process_config.input_paths:
            if not Path(input_path).is_dir():
                raise EODataflowReportError('Input path directory does not exist : {0}'.format(input_path))
            self.input_paths.append(Path(input_path))

        # Check root archive_path
        self.root_archive_path = Path(self.process_config.workspace_path)
        if not self.root_archive_path.is_dir():
            raise EODataflowReportError(
                'Root archive path directory does not exist : {0}'.format(self.root_archive_path),
            )
        self.root_archive_path = self.root_archive_path / 'archive'
        if not self.root_archive_path.is_dir():
            os.makedirs(self.root_archive_path)
            logger.info('Create archive space')

        # Check temporary ingestion folder
        self.temp_path = Path(self.process_config.workspace_path) / 'tmp'
        if not self.temp_path.is_dir():
            os.makedirs(self.temp_path)
            logger.info('Create temporary ingestion space')

        # Check rejected_path
        self.rejected_path = Path(self.process_config.workspace_path) / 'rejected'
        if not self.rejected_path.is_dir():
            os.makedirs(self.rejected_path)
            logger.info('Create rejection space')

    def move_files_to_temp_inject_folder(self) -> int:
        """
        Moves the files in input path matching the input pattern to the
        temporary inject folder.

        Returns:
            The total number of files moved.
        """
        logger.info('Move files to temporary ingestion folder')

        total_nb_files = 0
        for input_path in self.input_paths:
            for file in input_path.glob('*.json'):
                file_date_hour = datetime.datetime.strptime(
                    re.search('\d{10}', str(file.resolve())).group(),
                    '%Y%m%d%H',
                )
                current_date_hour = datetime.datetime.strptime(
                    datetime.datetime.strftime(datetime.datetime.now(), '%Y%m%d%H'),
                    '%Y%m%d%H',
                )
                age_condition = files.mtime_older_than(
                    file_path=file,
                    delta_value=self.process_config.harvesting_delay.value,
                    delta_unit=self.process_config.harvesting_delay.unit,
                )
                if file_date_hour < current_date_hour and age_condition:
                    logger.debug('Moving file : {0} to {1}'.format(file, self.temp_path))
                    try:
                        shutil.move(str(file.resolve()), self.temp_path)
                        total_nb_files += 1
                    except shutil.Error:
                        temp_dest = Path(self.temp_path, Path(file.resolve()))
                        if temp_dest.is_file():
                            logger.warning('File already exists : {0}'.format(temp_dest))
                        else:
                            logger.error('An error occurred while moving metrics file', exc_info=True)
        return total_nb_files

    def retrieve_es_index_name(self, file_name: str) -> str:
        """Retrieves the elasticsearch index from the file name.

        Args:
            file_name: The file name.
        Returns:
            str: The elasticsearch index name.
        Raises:
            EODataflowReportError: When the date cannot be extracted from the
            file name.
        """
        date_pattern = '%Y%m%d'
        try:
            file_date = datetime.datetime.strptime(
                re.search('\d{8}', file_name).group(),
                date_pattern,
            )
        except ValueError:
            msg = 'Unable to extract year and month from the filename: "{0}"'.format(file_name)
            logger.error(msg)
            raise EODataflowReportError(msg)
        es_index_name = self.process_config.elasticsearch.prefix + '_' + file_date.strftime('%Y%m')
        logger.info('Elasticsearch index for file {0} : {1}'.format(file_name, es_index_name))
        return es_index_name

    def move_harvested_file_to_archive(self, file: Path):
        """Moves successfully harvested file to archive

        Args:
            file: The path of the file to move.
        """

        # archive/{year}/{month}/{date}_cersat_downloader_metrics
        date_pattern = '%Y%m%d'
        file_date = datetime.datetime.strptime(re.search('\d{8}', str(file.resolve())).group(), date_pattern)

        archive_path = Path(
            self.root_archive_path /
            file_date.strftime('%Y') /
            file_date.strftime('%m') /
            '{0}_cersat_downloader_metrics'.format(file_date.strftime(date_pattern))
        )

        logger.info('Moving file {0} to archive : {1}'.format(file.resolve(), archive_path.resolve()))

        # If archive does not exist, create it
        if not Path('{0}.gz'.format(str(archive_path.resolve()))).is_file():
            # define and create destination path
            parent_dir_path = archive_path.parent
            if not parent_dir_path.is_dir():
                os.makedirs(parent_dir_path)

            destination_file_path = str(archive_path.resolve()) + '.gz'
            # zip file
            with open(str(file.resolve()), 'rb') as src, gzip.open(destination_file_path, 'wb') as zipped_file:
                zipped_file.writelines(src)

            # Delete source file
            os.remove(str(file.resolve()))

        # Else, update the archive
        else:
            with gzip.open(str(archive_path.resolve()) + '.gz', 'rb') as arch_content:
                existing_content = arch_content.read()
            with gzip.open(str(archive_path.resolve()) + '.gz', 'wb') as existing:
                with open(str(file.resolve()), 'r') as content_to_add:
                    existing.write(existing_content + content_to_add.read().encode())
            os.remove(str(file.resolve()))

    def prepare_bulk_inserts(
        self,
        file: Path,
        es_index_name: str,
    ) -> List[Tuple[str, Dict]]:
        """Reads the file line by line to build a list of dictionaries
        representing the data to push to elasticsearch.

        Args:
            file: The path of the file containing the json data to insert
            es_index_name: The elasticsearch index to push the data to

        Returns:
            A list of tuples of ids and dictionaries containing the data to
            index.
        """
        index_list = list()
        with open(str(file.resolve()), 'r') as data_file:
            for line in data_file:
                json_line = json.loads(line)
                doc = {
                       '_index': es_index_name,
                       '_id': '{0}_{1}_{2}'.format(
                            json_line['download'],
                            json_line['filename'].replace(' ', ''),
                            json_line['download_time'],
                        ),
                       '_source': json_line
                   }
                index_list.append((doc['_id'], doc))

        return index_list

    def process_metrics_file(self, file: Path, es_index_name: str):
        """Processes a metric file.\n
        Launches the inserts in bulk (list of dictionaries).
        Retrieves the bulk insert results and moves the file either to the
        rejection space (failure) or archive space (success) as defined in
        process configuration.

        Args:
            file: The path of the file to process.
            es_index_name: The elasticsearch index name.
        """
        logger.info('Attempt to ingest file {0} to elasticsearch'.format(file))

        inserts = self.prepare_bulk_inserts(file=file, es_index_name=es_index_name)

        if len(inserts) > 0:
            logger.info('Index {0} metrics from {1} to elasticsearch index {2} in bulk'.format(
                    len(inserts),
                    file,
                    es_index_name,
                ),
            )
            success, failed = self.es_client.index(
                index_path=es_index_name,
                docs=inserts,
                operation_key=ES_REPORT_INDEX,
                raise_error_on_failure=True,
            )
            logger.info('Success : {0} / {1}, Failed : {2}'.format(success, len(inserts), failed))
            if not failed:
                self.move_harvested_file_to_archive(file=file)
            else:
                logger.info('Moving file {0} to reject {1}'.format(file, self.rejected_path))
                shutil.move(str(file.resolve()), self.rejected_path)

    def process_harvested_files(self):
        """
        Processes harvested files previously placed in temporary ingestion
        folder.
        For each file:
            - retrieves the elasticsearch index name.
            - creates the index if not already existing.
            - processes the file.
        """
        # Process the files and index into Elasticsearch
        # Init ES client
        self.move_files_to_temp_inject_folder()
        self.es_client = ESClient(es_config=self.process_config.elasticsearch)
        es_mapping_path = Path(
            Path(__file__).parent.parent.parent,
            'share',
            'es',
            'eo_dataflow_report_mapping.json',
        )

        for file in self.temp_path.glob('*.json'):
            # retrieve targeted index name
            es_index_name = self.retrieve_es_index_name(file.name)
            # create index if not existing
            if not self.es_client.has_index(index_path=es_index_name):
                self.es_client.create_index(index_path=es_index_name, mappings_path=es_mapping_path)
            # process metrics file
            self.process_metrics_file(file=file, es_index_name=es_index_name)
