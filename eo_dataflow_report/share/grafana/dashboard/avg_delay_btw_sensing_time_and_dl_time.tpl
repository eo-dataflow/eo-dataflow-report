    {
      "datasource": {
        "type": {{datasourceType|jsonify}},
        "uid": {{datasourceUid|jsonify}}
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "delay (h)",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 0,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        },
        "overrides": [
          {
            "matcher": {
              "id": "byName",
              "options": "Average local_update_delay"
            },
            "properties": [
              {
                "id": "color",
                "value": {
                  "fixedColor": "orange",
                  "mode": "fixed"
                }
              }
            ]
          },
          {
            "matcher": {
              "id": "byName",
              "options": "Average total delay"
            },
            "properties": [
              {
                "id": "color",
                "value": {
                  "fixedColor": "orange",
                  "mode": "fixed"
                }
              }
            ]
          }
        ]
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 9
      },
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": {{timeField|jsonify}},
              "id": "2",
              "settings": {
                "interval": "auto",
                "min_doc_count": "0",
                "timeZone": "utc",
                "trimEdges": "0"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": {{datasourceType|jsonify}},
            "uid": {{datasourceUid|jsonify}}
          },
          "metrics": [
            {
              "field": "local_update_delay",
              "hide": false,
              "id": "3",
              "settings": {
                "script": "_value / 3600"
              },
              "type": "avg"
            },
            {
              "field": "production_latency_delay",
              "hide": false,
              "id": "4",
              "settings": {
                "script": "_value / 3600"
              },
              "type": "avg"
            }
          ],
          "query": "download: \"${download:raw}\"",
          "refId": "A",
          "timeField": {{timeField|jsonify}}
        }
      ],
      "title": "${download}: average delay between sensing time and download time, per sensing day",
      "transformations": [
        {
          "id": "calculateField",
          "options": {
            "alias": "Average total delay",
            "binary": {
              "left": "Average local_update_delay",
              "operator": "+",
              "reducer": "sum",
              "right": "Average production_latency_delay"
            },
            "mode": "binary",
            "reduce": {
              "include": [
                "Average local_update_delay",
                "Average production_latency_delay"
              ],
              "reducer": "sum"
            },
            "replaceFields": true
          }
        }
      ],
      "type": "timeseries"
    }
