    {
      "datasource": {
        "type": {{datasourceType|jsonify}},
        "uid": {{datasourceUid|jsonify}}
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "number of files",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "bars",
            "fillOpacity": 100,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 1
      },
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": "download_time",
              "id": "2",
              "settings": {
                "interval": "1d",
                "timeZone": "Europe/Paris"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": {{datasourceType|jsonify}},
            "uid": {{datasourceUid|jsonify}}
          },
          "metrics": [
            {
              "id": "1",
              "type": "count"
            }
          ],
          "query": "download: \"${download:raw}\"",
          "refId": "A",
          "timeField": {{timeField|jsonify}}
        }
      ],
      "title": "${download}: number of files downloaded per day",
      "type": "timeseries"
    }
