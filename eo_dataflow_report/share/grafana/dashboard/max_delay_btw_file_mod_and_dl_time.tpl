    {
      "datasource": {
        "type": {{datasourceType|jsonify}},
        "uid": {{datasourceUid|jsonify}}
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "delay (h)",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "line",
            "fillOpacity": 0,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 1,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        },
        "overrides": [
          {
            "matcher": {
              "id": "byName",
              "options": "Max local_update_delay"
            },
            "properties": [
              {
                "id": "color",
                "value": {
                  "fixedColor": "orange",
                  "mode": "fixed"
                }
              }
            ]
          }
        ]
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 12,
        "y": 9
      },
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": "download_time",
              "id": "3",
              "settings": {
                "interval": "1d",
                "min_doc_count": "0",
                "timeZone": "utc",
                "trimEdges": "0"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": {{datasourceType|jsonify}},
            "uid": {{datasourceUid|jsonify}}
          },
          "metrics": [
            {
              "field": "local_update_delay",
              "id": "1",
              "settings": {
                "script": "_value / 3600"
              },
              "type": "max"
            }
          ],
          "query": "download: \"${download:raw}\"",
          "refId": "A",
          "timeField": "sensing_time"
        }
      ],
      "title": "${download}: maximum delay between the file modification time and the download time, per day",
      "type": "timeseries"
    }
