    {
      "datasource": {
        "type": {{datasourceType|jsonify}},
        "uid": {{datasourceUid|jsonify}}
      },
      "description": "",
      "fieldConfig": {
        "defaults": {
          "color": {
            "mode": "palette-classic"
          },
          "custom": {
            "axisLabel": "number of files",
            "axisPlacement": "auto",
            "barAlignment": 0,
            "drawStyle": "bars",
            "fillOpacity": 33,
            "gradientMode": "none",
            "hideFrom": {
              "legend": false,
              "tooltip": false,
              "viz": false
            },
            "lineInterpolation": "linear",
            "lineWidth": 2,
            "pointSize": 5,
            "scaleDistribution": {
              "type": "linear"
            },
            "showPoints": "auto",
            "spanNulls": false,
            "stacking": {
              "group": "A",
              "mode": "none"
            },
            "thresholdsStyle": {
              "mode": "off"
            }
          },
          "displayName": "Count files",
          "mappings": [],
          "thresholds": {
            "mode": "absolute",
            "steps": [
              {
                "color": "green",
                "value": null
              },
              {
                "color": "red",
                "value": 80
              }
            ]
          }
        },
        "overrides": []
      },
      "gridPos": {
        "h": 8,
        "w": 12,
        "x": 0,
        "y": 1
      },
      "hideTimeOverride": false,
      "maxDataPoints": 24,
      "options": {
        "legend": {
          "calcs": [],
          "displayMode": "list",
          "placement": "bottom"
        },
        "tooltip": {
          "mode": "single",
          "sort": "none"
        }
      },
      "targets": [
        {
          "alias": "",
          "bucketAggs": [
            {
              "field": {{timeField|jsonify}},
              "id": "2",
              "settings": {
                "interval": "auto"
              },
              "type": "date_histogram"
            }
          ],
          "datasource": {
            "type": {{datasourceType|jsonify}},
            "uid": {{datasourceUid|jsonify}}
          },
          "metrics": [
            {
              "field": "filename.keyword",
              "id": "1",
              "type": "cardinality"
            }
          ],
          "query": "download: \"${download:raw}\"",
          "refId": "A",
          "timeField": {{timeField|jsonify}}
        }
      ],
      "title": "${download}: number of files downloaded per sensing day",
      "type": "timeseries"
    }
