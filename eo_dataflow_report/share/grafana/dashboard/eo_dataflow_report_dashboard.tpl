{
  "dashboard":
   {
     "panels": [
       {
         "collapsed": false,
         "gridPos": {
           "h": 1,
           "w": 24,
           "x": 0,
           "y": 0
         },
         "panels": [],
         "repeat": "download",
         "title": "${download}",
         "type": "row"
       },
       {# ------------------ 4 panels ------------------ #}
       {# ------------------ "${download}: number of files downloaded per sensing day" ------------------ #}
       {{nb_of_files_dl_per_sensing_day}},
       {# ------------------ "${download}: number of files downloaded per day" ------------------ #}
       {{nb_of_files_dl_per_day}},
       {# ------------------ "${download}: average delay between sensing time and download time, per sensing day" ------------------ #}
       {{avg_delay_btw_sensing_time_and_dl_time}},
       {# ------------------ "${download}: maximum delay between the file modification time and the download time, per day" ------------------ #}
       {{max_delay_btw_file_mod_and_dl_time}}
     ],
     "templating": {
       "list": [
         {
           "current": {
             "selected": true,
             "text": [
               "All"
             ],
             "value": [
               "$__all"
             ]
           },
           "hide": 0,
           "includeAll": true,
           "multi": true,
           "name": "download",
           "options": [
             {
               "selected": true,
               "text": "All",
               "value": "$__all"
             },
             {# ------------------ 1 per download in group, see templating_list_option.tpl ------------------ #}
             {{templating_list_options}}
           ],
           {# ------------------ esa_s1b_wv_ocn,\nesa_s1a_wv_ocn,\ncoda_s3b_sl_2_wst____o_nr,\ncoda_s3a_sl_2_wst____o_nr,\ncoda_s3b_sr_2_wat,\nOS_coda_s3a_sr_2_wat ------------------ #}
           "query": {{downloads_list|jsonify}},
           "queryValue": "",
           "skipUrlSync": false,
           "type": "custom"
         }
       ]
     },
     "style": "dark",
     "time": {
       "from": "now-{{dashboardTimeRangeFrom|jsonify}}d",
       "to": "now-{{dashboardTimeRangeTo|jsonify}}d"
     },
     "title": "eo-dataflow {{group_name}}"
     {# ------------------ in case of update ------------------ #}
     {%- if dashboard_uid %}
     ,
     "uid":{{dashboard_uid|jsonify}}
     {%- endif %}
   },
   "folderId": {{folderId|jsonify}},
   "overwrite": {{overwrite|jsonify}}
}
