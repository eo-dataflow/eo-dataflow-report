{
   "name":"Number of files downloaded per day for {{group_name}}, {{download_name}}",
   "interval":{{alertIntervall|jsonify}},
   "rules":[
      {
         "grafana_alert":{
            "title":"Number of files downloaded per day for {{group_name}}, {{download_name}}",
            "condition":"B",
            "no_data_state":"NoData",
            "exec_err_state":"Alerting",
            "data":[
               {
                  "refId":"A",
                  "queryType":"",
                  "relativeTimeRange":{
                     "from":{{alertTimeRangeFrom|jsonify}},
                     "to":{{alertTimeRangeTo|jsonify}}
                  },
                  "datasourceUid":{{datasourceUid|jsonify}},
                  "model":{
                     "alias":"",
                     "bucketAggs":[
                        {
                           "field":"download_time",
                           "id":"2",
                           "settings":{
                              "interval":"1d",
                              "timeZone":"Europe/Paris"
                           },
                           "type":"date_histogram"
                        }
                     ],
                     "datasource":{
                        "type":"elasticsearch",
                        "uid":{{datasourceUid|jsonify}}
                     },
                     "metrics":[
                        {
                           "id":"1",
                           "type":"count"
                        }
                     ],
                     "query":"download: {{download_name}}",
                     "refId":"A",
                     "timeField":{{timeField|jsonify}},
                     "intervalMs":86400000
                  }
               },
               {
                  "refId":"B",
                  "datasourceUid":"-100",
                  "queryType":"",
                  "model":{
                     "refId":"B",
                     "hide":false,
                     "type":"classic_conditions",
                     "datasource":{
                        "uid":"-100",
                        "type":"__expr__"
                     },
                     "conditions":[
                        {
                           "type":"query",
                           "evaluator":{
                              "params":[
                                 {{alert_threshold}}
                              ],
                              "type":"lt"
                           },
                           "operator":{
                              "type":"and"
                           },
                           "query":{
                              "params":[
                                 "A"
                              ]
                           },
                           "reducer":{
                              "params":[

                              ],
                              "type":"last"
                           }
                        }
                     ]
                  }
               }
            ]
         },
         "for":{{alertFor|jsonify}},
         "annotations":{
            "__dashboardUid__":{{dashboard_uid|jsonify}},
            "__panelId__": "3"
         },
         "labels":{
            "application_name": "eo_dataflow_report",
            "group_name":{{group_name|jsonify}},
            "download_name":{{download_name|jsonify}},
            "dashboard_uid":{{dashboard_uid|jsonify}}
         }
      }
   ]
}
