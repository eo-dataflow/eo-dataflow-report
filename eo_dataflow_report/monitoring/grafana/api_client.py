"""Grafana API client."""

import json
import logging
from http import HTTPStatus
from typing import Any, Dict, List, NoReturn, Optional, Union

import requests
from grafana_client import GrafanaApi
from grafana_client.client import GrafanaClientError

from eo_dataflow_report.monitoring.grafana.exceptions import GrafanaApiClientError, GrafanaError, GrafanaHttpClientError
from eo_dataflow_report.monitoring.grafana.models import (
    GrafanaCreateElasticsearchDatasource,
    GrafanaCreateJsonData,
    GrafanaFolder,
    SecretModel,
)
from eo_dataflow_report.utils.configuration import GrafanaConfiguration


class GrafanaAPIClient(object):
    """Grafana API client."""

    def __init__(self, grafana_config: Optional[GrafanaConfiguration] = None):
        """Initialize the GrafanaAPIClient.

        Args:
            grafana_config: The Grafana configuration
        """
        super().__init__()
        self.config = grafana_config if grafana_config else GrafanaConfiguration()
        self.client = self.init_client()
        self.log = logging.getLogger('eo_dataflow_report')
        self.headers = {
            'Authorization': 'Bearer {0}'.format(self.config.api_key),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        }
        self.protocol = 'https' if self.config.ssl_verify else 'http'

    def init_client(self) -> GrafanaApi:
        """Initialize the client.

        Returns:
            A GrafanaApi instance
        """
        return GrafanaApi(host=self.config.host, auth=self.config.api_key, verify=self.config.ssl_verify)

    def datasource_exists(self, name: str) -> bool:
        """Determine from given name if a Grafana datasource exists.

        Args:
            name: The datasource name to check

        Returns:
            A boolean indicating whether the Grafana datasource exists

        Raises:
            GrafanaError: In case of Grafana python API error
        """
        try:
            self.client.datasource.get_datasource_by_name(datasource_name=name)
        except GrafanaClientError as error:
            if error.status_code == HTTPStatus.NOT_FOUND:
                return False
        except Exception as exception:
            raise GrafanaError('Error while retrieving Grafana datasource by name: {0}'.format(exception))
        return True

    def create_grafana_es_datasource(self, name: str, es_index_name: str, time_field: str) -> NoReturn:
        """Create a Grafana datasource using API client.

        Args:
            name: The datasource name
            es_index_name: The Elasticsearch index name
            time_field: The time field

        Raises:
            GrafanaApiClientError: In case a datasource with the same name already exists
            GrafanaError: In case of Grafana python API error
        """
        if self.datasource_exists(name=name):
            raise GrafanaApiClientError('Data source with the name {0} already exists'.format(name))
        grafana_datasources_config = self.config.datasources
        create_data_source_object = GrafanaCreateElasticsearchDatasource(
            name=name,
            type=grafana_datasources_config.type,
            access=grafana_datasources_config.access,
            url=grafana_datasources_config.url,
            password=SecretModel(password=grafana_datasources_config.password),
            user=grafana_datasources_config.user,
            database=es_index_name,
            basic_auth=grafana_datasources_config.basic_auth,
            json_data=GrafanaCreateJsonData(
                es_version=self.config.datasources.json_data.es_version,
                include_frozen=self.config.datasources.json_data.include_frozen,
                max_concurrent_shard_requests=self.config.datasources.json_data.max_concurrent_shard_requests,
                time_field=time_field,
                time_interval=self.config.datasources.json_data.time_interval,
            ),
        )
        try:
            create_result = self.client.datasource.create_datasource(datasource=create_data_source_object.to_api_dict())
            self.log.info(
                'Message : {0}, id: {1}, name: {2}, datasource : {3}'.format(
                    create_result['message'],
                    create_result['id'],
                    create_result['name'],
                    json.dumps(create_result['datasource'], indent=2),
                ),
            )
        except Exception as exception:
            raise GrafanaError('Error while creating Grafana datasource : {0}'.format(exception))

    def list_folders(self) -> List[GrafanaFolder]:
        """List existing Grafana folders.

        Returns:
            The Grafana folders list

        Raises:
            GrafanaError: In case of Grafana python API error
        """
        folders = []
        try:
            folders_result = self.client.folder.get_all_folders()
            for folder in folders_result:
                folders.append(
                    GrafanaFolder(
                        id=folder['id'],
                        uid=folder['uid'],
                        title=folder['title'],
                    ),
                )
        except Exception as exception:
            raise GrafanaError('Error while listing Grafana folders : {0}'.format(exception))
        return folders

    def get_root_folder_id(self) -> int:
        """Retrieve the Grafana root folder id.

        Returns:
            The Grafana root folder id

        Raises:
            GrafanaApiClientError: In case the folder does not exist
        """
        root_folder_name = self.config.root_folder
        folders = self.list_folders()
        filtered = list(filter(lambda folder: folder.title == root_folder_name, folders))
        if filtered and isinstance(filtered[0], GrafanaFolder):
            return filtered[0].id
        raise GrafanaApiClientError('No folder named {0}'.format(root_folder_name))

    def folder_exists(self, folder_name: str) -> bool:
        """Check whether a given folder name exists.

        Args:
            folder_name: The folder name

        Returns:
            A boolean indicating whether the given folder name exists
        """
        folders_filtered = filter(lambda folder: folder.title == folder_name, self.list_folders())
        nb_folders = len(list(folders_filtered))
        return nb_folders > 0

    def create_root_folder(self) -> GrafanaFolder:
        """Create the Grafana root folder.

        Returns:
            The created instance of GrafanaFolder

        Raises:
            GrafanaError: In case of Grafana API error
            GrafanaApiClientError: In case the root folder already exists
        """
        root_folder_name = self.config.root_folder
        if self.folder_exists(folder_name=root_folder_name):
            raise GrafanaApiClientError('A folder named {0} already exists'.format(root_folder_name))
        else:
            try:
                create_result = self.client.folder.create_folder(title=root_folder_name)
                self.log.info('Folder created : \n {0}'.format(json.dumps(create_result, indent=2)))
                return GrafanaFolder(
                    id=create_result['id'],
                    uid=create_result['uid'],
                    title=create_result['title'],
                )
            except Exception as exception:
                raise GrafanaError('Error while creating root folder : {0}'.format(exception))

    def create_dashboard(self, dashboard_json: str) -> str:
        """Create a Grafana dashboard.

        Args:
            dashboard_json: The json formatted string representing the dashboard

        Returns:
            The dashboard uid

        Raises:
            GrafanaError: In case of Grafana API error
        """
        try:
            create_result = self.client.dashboard.update_dashboard(dashboard=dashboard_json)
            self.log.info('Dashboard created : \n {0}'.format(json.dumps(create_result, indent=2)))
            return create_result['uid']
        except Exception as exception:
            raise GrafanaError('Error while creating dashboard : {0}'.format(exception))

    def list_dashboards(self) -> List[dict]:
        """List existing Grafana dashboards.

        Returns:
            The Grafana dashboards list

        Raises:
            GrafanaError: In case of Grafana API error
        """
        try:
            return self.client.search.search_dashboards(type_=self.config.search.dashboard_type)
        except Exception as exception:
            raise GrafanaError('Error while listing dashboards : {0}'.format(exception))

    def dashboard_exists(self, dashboard_name: str) -> bool:
        """Check whether a given dashboard name exists in the root folder.

        Args:
            dashboard_name: The dashboard name

        Returns:
            A boolean indicating whether the given dashboard name exists
        """
        for dashboard in self.list_dashboards():
            if dashboard['title'] == dashboard_name:
                if 'folderId' in dashboard.keys() and dashboard['folderId'] == self.get_root_folder_id():
                    return True
        return False

    def get_dashboard_uid_by_name(self, dashboard_name: str) -> str:
        """Get a dashboard uid by name (in the root folder).

        Args:
            dashboard_name: The dashboard name

        Returns:
            The dashboard uid

        Raises:
            GrafanaApiClientError: In case the dashboard name does not exist
        """
        for dashboard in self.list_dashboards():
            if dashboard['title'] == dashboard_name:
                if 'folderId' in dashboard.keys() and dashboard['folderId'] == self.get_root_folder_id():
                    return dashboard['uid']
        raise GrafanaApiClientError('No dashboard named {0}'.format(dashboard_name))

    def create_alert_rule(self, alert_rule: str) -> NoReturn:
        """Create Grafana alert rule.

        Args:
            alert_rule: The json formatted alert rule

        Raises:
            GrafanaError: In case of Grafana API error
        """
        try:
            create_result = self.client.alerting.create_alertrule(
                folder_name=self.config.root_folder,
                alertrule=json.loads(alert_rule),
            )
            self.log.info('{0}'.format(json.dumps(create_result, indent=2)))
        except Exception as exception:
            raise GrafanaError('Error while creating alert : {0}'.format(exception))

    def get_alert_rules_by_dashboard_uid(self, dashboard_uid: str) -> List[str]:
        """Get root folder Grafana alert rules labeled with the passed dashboard uid.

        Args:
            dashboard_uid: The dashboard_uid

        Returns:
            The alert names list

        Raises:
            GrafanaError: In case of Grafana API error
        """
        alerts_list = []
        try:
            response = self.client.alerting.get_alertrule(
                folder_name=self.config.root_folder,
                alertrule_name='',
            )
            if self.config.root_folder in response.keys():
                for alert in response[self.config.root_folder]:
                    if alert['rules'][0]['labels']['dashboard_uid'] == dashboard_uid:
                        alerts_list.append(alert['name'])
        except Exception as exception:
            raise GrafanaError('Error while retrieving alerts list  : {0}'.format(exception))
        return alerts_list

    def delete_alert_rules(self, dashboard_uid: str) -> NoReturn:
        """Delete all Grafana alert rules labeled with the passed dashboard uid.

        Args:
            dashboard_uid: The dashboard_uid

        Raises:
            GrafanaError: In case of Grafana API error
        """
        for alert_name in self.get_alert_rules_by_dashboard_uid(dashboard_uid=dashboard_uid):
            try:
                response = self.client.alerting.delete_alertrule(
                    folder_name=self.config.root_folder,
                    alertrule_name=alert_name,
                )
                self.log.info('Alert rule deleted : \n {0}'.format(json.dumps(response, indent=2)))
            except Exception as exception:
                raise GrafanaError('Error while deleting alert : {0}'.format(exception))

    def get_notification_policies(self) -> Dict[str, Any]:
        """Retrieve the notification policies' tree.

        Returns:
            A dictionary representing the notification policies' tree

        Raises:
            GrafanaError: In case of Grafana API error
        """
        try:
            response = self.client.alertingprovisioning.get_notification_policy_tree()
            return response
        except Exception as exception:
            raise GrafanaError('Error while retrieving notification policies : {0}'.format(exception))

    @staticmethod
    def get_contact_point_index(
        existing_contact_points: List[Dict[Any, Any]],
        contact_point_name: str,
    ) -> Union[int, None]:
        """Get the passed contact point name index.
        Determine whether a contact with the passed named already exists in the passed list.

        Args:
            existing_contact_points: The contact points list
            contact_point_name: The contact point name

        Returns:
            The contact point index if existing else None
        """
        for index, contact_point in enumerate(existing_contact_points):
            if contact_point['name'] == contact_point_name:
                return index
        return None

    def create_contact_point(self, contact_point: Dict[str, Any]) -> NoReturn:
        """Create a Grafana contact point if not already existing.

        Args:
            contact_point: A Grafana compliant dictionary representing the contact point.

        Raises:
            GrafanaError: In case of Grafana API error
        """
        existing_contact_points = self.client.alertingprovisioning.get_contactpoints()
        if not self.get_contact_point_index(
                existing_contact_points=existing_contact_points,
                contact_point_name=contact_point['name'],
        ):
            try:
                response = self.client.alertingprovisioning.create_contactpoint(contactpoint=contact_point)
                self.log.info('Contact point created : \n {0}'.format(json.dumps(response, indent=2)))
            except Exception as exception:
                raise GrafanaError('Error while creating contact point: {0}'.format(exception))
        else:
            self.log.info('A contact point named {0} already exists'.format(contact_point['name']))

    def delete_contact_point(self, contact_point_name: str) -> NoReturn:
        """Delete a contact point by name.

        Args:
            contact_point_name: The contact point name

        Raises:
            GrafanaError: In case of Grafana API error
        """
        contact_point_uid = None
        existing_contact_points = self.client.alertingprovisioning.get_contactpoints()
        for existing_contact_point in existing_contact_points:
            if existing_contact_point['name'] == contact_point_name:
                contact_point_uid = existing_contact_point['uid']
                break
        if contact_point_uid:
            try:
                response = self.client.alertingprovisioning.delete_contactpoint(contactpoint_uid=contact_point_uid)
                self.log.info('Contact point deleted : \n {0}'.format(json.dumps(response, indent=2)))
            except Exception as exception:
                raise GrafanaError('Error while deleting contact point: {0}'.format(exception))
        else:
            self.log.info('No contact point named {0}'.format(contact_point_name))

    def create_notification_policy(self, notification_policy: Dict[str, Any]) -> NoReturn:
        """Create a Grafana notification policy.

        Args:
            notification_policy: The notification policy to add (route)

        Raises:
            GrafanaError: In case of Grafana API error
        """
        notification_policies = self.get_notification_policies()
        if 'routes' in notification_policies.keys():
            notification_policies['routes'].append(notification_policy)
        else:
            notification_policies['routes'] = [notification_policy]
        try:
            response = self.client.alertingprovisioning.set_notification_policy_tree(
                notification_policy_tree=notification_policies,
            )
            self.log.info('Notification policy created : \n {0}'.format(json.dumps(response, indent=2)))
        except Exception as exception:
            raise GrafanaError('Error while creating notification policy: {0}'.format(exception))

    @staticmethod
    def get_route_index(
        existing_routes: List[Dict[Any, Any]],
        contact_point_name: str,
    ) -> Union[int, None]:
        """Get the passed route index matching the passed contact point name.
        Determine whether a route matching with the passed contact point name exists in the passed list.

        Args:
            existing_routes: The routes list
            contact_point_name: The contact point name

        Returns:
            The route index if existing else None
        """
        for index, route in enumerate(existing_routes):
            if route['receiver'] == contact_point_name:
                return index
        return None

    def delete_notification_policies(self, contact_point_names: List[str]) -> NoReturn:
        """Delete notification policies.

        Args:
            contact_point_names: The contact points names

        Raises:
            GrafanaError: In case of Grafana API error
        """
        notification_policies = self.get_notification_policies()
        if 'routes' in notification_policies.keys():
            nb_routes = len(notification_policies['routes'])
            if nb_routes > 0:
                for contact_point_name in contact_point_names:
                    route_index = self.get_route_index(
                        existing_routes=notification_policies['routes'],
                        contact_point_name=contact_point_name,
                    )
                    if route_index is not None:
                        notification_policies['routes'].pop(route_index)
                if len(notification_policies['routes']) < nb_routes:
                    try:
                        response = self.client.alertingprovisioning.set_notification_policy_tree(
                            notification_policy_tree=notification_policies,
                        )
                        self.log.info('Notification policies deleted : \n {0}'.format(json.dumps(response, indent=2)))
                    except Exception as exception:
                        raise GrafanaError('Error while deleting notification policies: {0}'.format(exception))
