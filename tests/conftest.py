import logging
import os
from pathlib import Path
from typing import List

from _pytest.fixtures import fixture

from eo_dataflow_report.index.es.es_client import ESClient
from eo_dataflow_report.utils.configuration import (
    EODataflowReportConfig,
    load_eo_dataflow_report_config)


def setup_logging():
    """Loggers setup."""

    es_logger = logging.getLogger('elasticsearch')
    es_logger.setLevel(logging.WARNING)

    report_logger = logging.getLogger('eo_dataflow.report')
    report_logger.setLevel(logging.WARNING)


@fixture(scope='session')
def config_file(test_dir: Path) -> Path:
    """Path to configuration file fixture.

    Args:
        test_dir: The test directory path

    Returns:
        Path to configuration file
    """
    return test_dir / 'resources/eo_dataflow_config.yaml'


@fixture(scope='session')
def process_config(config_file: Path) -> EODataflowReportConfig:
    """Setup configuration file fixture.

    Args:
        config_file: The configuration file path

    Returns:
        An instance of EO dataflow report configuration.
    """
    os.environ['EO_DATAFLOW_REPORT_CFG'] = str(config_file.absolute())
    return load_eo_dataflow_report_config(str(config_file.absolute()))


@fixture(scope='session')
def test_dir() -> Path:
    """Path to test dir fixture.

    Returns:
        Path to test dir
    """
    return Path(__file__).parent


@fixture(scope='session')
def output_dir(test_dir: Path) -> Path:
    """Path to output dir fixture.

    Args:
        test_dir: The test directory path

    Returns:
        Path to output dir
    """
    return test_dir / '_build'


@fixture(scope='session')
def resources_dir(test_dir: Path) -> Path:
    """Path to resource dir fixture.

    Args:
        test_dir: The test directory path

    Returns:
        Path to resource dir
    """
    return test_dir / 'resources'


@fixture(scope='session')
def index_suffix_list() -> List[str]:
    """List of the ES indices suffixes.

    Returns:
        The list of ES indices suffixes
    """
    return list(['201906', '202203'])


@fixture(scope='session', autouse=True)
def clean_indexes_teardown(index_suffix_list: List[str], process_config: EODataflowReportConfig):
    """Delete all indices created in Elasticsearch at the end of all tests.

    Args:
        index_suffix_list: The index suffixes list
        process_config: The process configuration
    """
    yield None

    # Create Elasticsearch client
    es_client = ESClient(es_config=process_config.elasticsearch)

    # For each index, delete index
    for suffix in index_suffix_list:
        index_path = '{0}{1}'.format(process_config.elasticsearch.prefix, suffix)
        # Check if the index exists before deleting
        if es_client.has_index(index_path):
            es_client.delete_index(index_path)
