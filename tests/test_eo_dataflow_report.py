import os
import shutil
from pathlib import Path
from typing import List

import pytest

from eo_dataflow_report.cli.create_dashboards import create_dashboards
from eo_dataflow_report.cli.index_reports import index_reports
from eo_dataflow_report.utils.configuration import (
    EODataflowReportConfig,
    load_eo_dataflow_report_config)


@pytest.fixture(scope='module')
def eo_dataflow_index_reports_args(
    resources_dir: Path,
    output_dir: Path,
    config_file: Path,
    process_config: EODataflowReportConfig,
    test_dir: Path,
) -> List[str]:
    """Set the process arguments.

    Args:
        resources_dir: The resources directory
        output_dir: The output directory
        config_file: The configuration file
        process_config: The process configuration
        test_dir: The test directory

    Returns:
        The arguments list
    """

    # Read the directories from the process config file.
    cfg = load_eo_dataflow_report_config(config_file)
    inputs = cfg.input_paths
    workspace_path = cfg.workspace_path.name

    # Create workspace test dir
    if not (output_dir / workspace_path).is_dir():
        os.makedirs(output_dir / workspace_path)

    # Copy the input dirs to working directory
    # 'tests/resources/dist_dirs/dir1/metrics'
    # 'tests/resources/workspace'
    dest_paths = list()
    for input_dir in inputs:
        rel_path = os.path.relpath(input_dir.absolute(), start='tests/resources')
        dest_path = output_dir.joinpath(str(rel_path))
        shutil.copytree(test_dir.parent / input_dir, dest_path.resolve(), dirs_exist_ok=True)
        dest_paths.append(str(dest_path.resolve()))

    # Set the arguments list
    args_list = list(['-C', 'workspace_path=' + str((output_dir / workspace_path).resolve())])
    args_list.append('input_paths=[' + ','.join(dest_paths) + ']')
    return args_list


@pytest.fixture(scope='module')
def eo_dataflow_create_dashboards_args(config_file: Path) -> List[str]:
    """ Set the process arguments from configuration file.

    Args:
        config_file: The configuration file

    Returns:
        The arguments list.
    """
    args_list = list(['-c', str(config_file)])
    return args_list


def test_eo_dataflow_report(eo_dataflow_index_reports_args: List[str], eo_dataflow_create_dashboards_args: List[str]):
    """Test EO dataflow index reports and create dashboards.

    Args:
        eo_dataflow_index_reports_args: The eo_dataflow_index_reports process arguments
        eo_dataflow_create_dashboards_args: The eo_dataflow_create_dashboards process arguments
    """
    print('test_eo_dataflow_index_reports')

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        index_reports(eo_dataflow_index_reports_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0

    print('test_eo_dataflow_create_dashboards')

    with pytest.raises(SystemExit) as pytest_wrapped_e:
        create_dashboards(eo_dataflow_create_dashboards_args)

    assert pytest_wrapped_e.type == SystemExit
    assert pytest_wrapped_e.value.code == 0
