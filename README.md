# eo-dataflow-report

## Development

### Install dependencies

- installer les dépendances et le projet

```bash
poetry install -vv
```

### Pre-commit

- register pre-commit

```bash
pre-commit install
```

- Run the hooks manually

```bash
pre-commit run --all-files
```

### Check code quality

```bash
flake8 .
```

### Run unit tests

```bash
pytest --tb=line
```

## Setup the system configuration
```bash
export EO_DATAFLOW_REPORT_CFG=<project_dir>/tests/resources/eo_dataflow_config.yaml
```

## Launch application

The application has two endpoints : `eo-dataflow-index-reports` and `eo-dataflow-create-dashboards`.

### eo-dataflow-index-reports

```bash
usage: eo-dataflow-index-reports [-h] [-c CONFIGURATION_FILE] [--logfile LOGFILE]
                                 [--logfile_level LOGFILE_LEVEL] [-v | -q | -s]
                                 [-P [key=value [key=value ...]]]
                                 [-C [key=value [key=value ...]]]

Collect downloaded eo files metrics and index them into elasticsearch indices.
The input is a configuration file.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                        Configuration file path
  --logfile LOGFILE     Path of the file where logs will be written
  --logfile_level LOGFILE_LEVEL
                        Minimal level that log messages must reach to be written in the log file
  -v, --verbose         Activate debug level logging - for extra feedback.
  -q, --quiet           Disable information logging - for reduced feedback.
  -s, --silence         Log ONLY the most critical or fatal errors.
  -P [key=value [key=value ...]]
                        Update of parameter items
  -C [key=value [key=value ...]]
                        Update of configuration items
```
### eo-dataflow-create-dashboards
```bash
usage: eo-dataflow-create-dashboards [-h] [-c CONFIGURATION_FILE] [--logfile LOGFILE]
                                     [--logfile_level LOGFILE_LEVEL] [-v | -q | -s]
                                     [-P [key=value [key=value ...]]]
                                     [-C [key=value [key=value ...]]]

Create dashboards from metrics indexed into Elasticsearch.
The input is a configuration file.

optional arguments:
  -h, --help            show this help message and exit
  -c CONFIGURATION_FILE, --configuration_file CONFIGURATION_FILE
                        Configuration file path
  --logfile LOGFILE     Path of the file where logs will be written
  --logfile_level LOGFILE_LEVEL
                        Minimal level that log messages must reach to be written in the log file
  -v, --verbose         Activate debug level logging - for extra feedback.
  -q, --quiet           Disable information logging - for reduced feedback.
  -s, --silence         Log ONLY the most critical or fatal errors.
  -P [key=value [key=value ...]]
                        Update of parameter items
  -C [key=value [key=value ...]]
                        Update of configuration items
```

### Dashboards, downloads, alerts and notifications configuration.
See example in `eo_dataflow_config.yaml`.

## Dashboards
Set the dashboards' root folder using `grafana.root_folder` option.

Set the dashboards' time range using the following options:
- `grafana.dashboards.time_range_from_days`, set to now -90 days by default
- `grafana.dashboards.time_range_to_days`, set to now by default

## Downloads
Configure downloads and their monitoring options in the `grafana.dashboards.groups` section.

Example :

```yaml
    groups:
      - name: 'cwwic cfosat'
        downloads:
          - name: 'cwwic_cfosat_swisca_l2s'
          - name: 'cwwic_cfosat_swi_l1a'
      - name: 'sentinel'
        downloads:
          - name : 'esa_s1b_wv_ocn'
          - name: 'esa_s1a_wv_ocn'
```

1 dashboard per group under the root folder and 4 panels by download are created :
- number of files downloaded per sensing day
- number of files downloaded per day
- average delay between sensing time and download time, per sensing day
- maximum delay between the file modification time and the download time, per day

## Alerts
Two optional alerts can be configured by adding a `monitoring` section on the following panels :

- number of files downloaded per sensing day

This alert triggers when the number of files downloaded per sensing day is below the value defined in `alert_threshold_nb_of_files_downloaded_per_sensing_day`

- number of files downloaded per day

This alert triggers when the number of files downloaded per day is below the value defined in `alert_threshold_nb_of_files_downloaded_per_day`

Example :
```yaml
      - name: 'cwwic cfosat'
        downloads:
          - name: 'cwwic_cfosat_swisca_l2s'
            monitoring:
              alert_threshold_nb_of_files_downloaded_per_sensing_day: 14
              alert_threshold_nb_of_files_downloaded_per_day: 1
          - name: 'cwwic_cfosat_swi_l1a'
            monitoring:
              alert_threshold_nb_of_files_downloaded_per_sensing_day: 17
              alert_threshold_nb_of_files_downloaded_per_day: 1
      - name: 'sentinel'
        downloads:
          - name : 'esa_s1b_wv_ocn'
            monitoring:
              alert_threshold_nb_of_files_downloaded_per_sensing_day:
              alert_threshold_nb_of_files_downloaded_per_day: 1
          - name: 'esa_s1a_wv_ocn'
            monitoring:
              alert_threshold_nb_of_files_downloaded_per_sensing_day: 500
              alert_threshold_nb_of_files_downloaded_per_day: 1
```

Set the `alert_threshold_<name>` value to -1 to disable an alert.

The alerts are displayed in the Grafana UI in the `Alert rules` page under the defined `root_folder`.

In addition to the Grafana native filters (data source, State, Rule type...), alerts can also be filtered by labels :
- application_name
- group_name
- download_name
- dashboard_uid

Example of query to enter in `Search by label`:
```
{application_name="eo_dataflow_report", group_name="cwwic cfosat"}
```


## Notifications
Configure optional `contact_points` section to receive mail notifications on alerts.

By default the repeat interval is set to 12 hours and the `disable_resolve_message` option is set to False.

```yaml
contact_points:
  - name: 'name1'
    addresses:
      - 'your.address@domain.com'
      - 'your.other.address@domain.com'
    disable_resolve_message: False
    repeat_interval:
      duration: 12
      unit: 'h'

  - name: 'name2'
    addresses:
      - 'your.address@domain.com'
      - 'your.other.address@domain.com'
    disable_resolve_message: False
    repeat_interval:
      duration: 12
      unit: 'h'
```

This will create `Contact points` and `Notification policies` displayed in the Grafana UI `Alerting` page.
